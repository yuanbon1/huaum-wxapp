import App from './App';
import { createSSRApp } from 'vue';
//全局方法
import Apis from "./ways/apis";
import Base from "./ways/base";
import Func from "./ways/func";
// 全局组件
import BasePage from '@/components/base/page.vue';
import BaseLoading from '@/components/base/loading.vue';
import BaseDialog from '@/components/base/dialog.vue';
import BasePopup from '@/components/base/popup.vue';
import BaseCard from '@/components/base/card.vue';
import BaseUpImage from '@/components/base/upimage.vue';

export function createApp() {
  const app = createSSRApp(App);
  // 自动注册全局方法
  app.config.globalProperties.$Apis = Apis;
  app.config.globalProperties.$Base = Base;
  app.config.globalProperties.$Func = Func;
  // 全局组件
  app.component("BasePage", BasePage);
  app.component("BaseLoading", BaseLoading);
  app.component("BaseDialog", BaseDialog);
  app.component("BasePopup", BasePopup);
  app.component("BaseCard", BaseCard);
  app.component("BaseUpImage", BaseUpImage);
  
  uni.$zp = {
  	config: {
  		'loadingMoreNoMoreText': '没有更多数据了',
  	}
  }
  
  return {
    app
  }
}