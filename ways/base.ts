import Apis from "./apis";
import Func from "./func";

export default {
  // 通过openid登录
  async LoginByOpenID() {
  	let that = this;
  	return new Promise((resolve, reject) => {
  		return that.LoginCode().then((code: string) => {
  			that.Post("BASE_AuthByOpenID", { code }, "POST")
        .then((res: any) => {
          // console.log(res);
  				if (res.code == 0) {
  					console.log(" === AuthByOpenID ===", res.data);
  					uni.setStorageSync("USER_INFO", res.data.userInfo);
  					uni.setStorageSync("TOKEN", res.data.token);
  					resolve(res.data);
  				} else {
            reject(res);
          }
  			}).catch((err: any) => {
          // console.log(err);
          reject(err);
        })
  		})
  	}).catch((err: any) => {
      // console.log(err);
      uni.setStorageSync("ERROR_INFO", err);
      that.RePage("/pages/error");
    })
  },
  // 获取code
  LoginCode() {
  	let that = this;
  	that.GetProvider();
  	return new Promise((resolve, reject) => {
  		uni.login({
  			provider: uni.getStorageSync("SYSTEM_PROVIDER_OAUTH"),
  			success: data => {
  				if (data.code) { resolve(data.code) } 
  				else { reject(data) }
  			}
  		});
  	});
  },
  // 获取服务类型
  GetProvider(type: any = 'oauth') {
  	uni.getProvider({
  		service: type,
  		success: data => {
        if(type == 'oauth') {
          uni.setStorageSync("SYSTEM_PROVIDER_OAUTH", data.provider[0]);
        }
        if(type == 'payment') {
          uni.setStorageSync("SYSTEM_PROVIDER_PAYMENT", data.provider[0]);
        }
  		}
  	})
  },
  Post(key: string = "", data = {}) {
    let that = this;
  	return new Promise((resolve, reject) => {
      let extInfo = { addon: Apis.MainAddon, addonid: Apis.Addonid };
  		uni.request({url: Apis[key], data, method: "POST",
  			header: Object.assign({
  				"content-type": "application/json",
  				"token": uni.getStorageSync("TOKEN"),
  				"provider": uni.getStorageSync("SYSTEM_PROVIDER_OAUTH"),
  			}, extInfo),
  			success: (res: any) => {
          if(res.statusCode == 200) {
            if(res.data.code === 0) {
              console.log(res.data);
              resolve(res.data);
            }else if(res.data.code === 401){
            	// 用户没有登陆
            	that.RePage("/pages/index");
            }else {
            	uni.setStorageSync("ERROR_INFO", res.data);
            	that.RePage("/pages/error");
            }
          }else {
            let msg = res.data.statusCode + ' ' + res.data.error;
            let obj = { code: 600, msg };
            uni.setStorageSync("ERROR_INFO", obj);
            that.RePage("/pages/error");
          }
  			},
  			fail: (err: any) => {
          // console.log(err);
          let obj = { code: 600, msg: err.errMsg };
          if(['BASE_AuthByOpenID'].includes(key)){
            reject(obj);
          }else {
            uni.setStorageSync("ERROR_INFO", obj);
            that.RePage("/pages/error");
          }
        },
  		});
  	});
  },
  // 关闭所有页面 跳转到某一页面
  RePage(url: string) {
    uni.reLaunch({ url })
  },
  // 返回上级页面
  BackPage(delta = 1) {
    uni.navigateBack({ delta })
  },
  // 跳转页面
  JumpPage(url: string) {
    uni.navigateTo({ url })
  },
  // 获取默认的屏幕高度
  GetScreenHeightDefault() {
    let system = uni.getWindowInfo(); // 系统信息
    let bouning: any = {};
    bouning = wx.getMenuButtonBoundingClientRect(); // 将囊坐标的 函数返回值
    // console.log(system);
    // custom = bouning && bouning.top && bouning.bottom ? bouning : OPTI.AppDefaultCustom; // 胶囊坐标
    
    // 开始计算全局高度
    let Height: any = {};
    Height.Custom = bouning; // 胶囊的坐标
    Height.SafeArea = system.safeAreaInsets; // 安全区域的坐标
    Height.StatusBar = system.statusBarHeight; // 状态栏高度
    
    Height.CustomBar = (bouning.top - Height.StatusBar) * 2 + bouning.height + Height.StatusBar; // 顶栏高度 包括状态栏
    Height.TitleBar = Height.CustomBar - Height.StatusBar; // 顶栏高度 不包括状态栏
    Height.ScreenHeight = system.screenHeight; // 可视窗口的高度
    
    // // 全端匹配底部安全区域高度 // 安全区域的高度
    // // (['ios','devtools'].indexOf(system.platform) > -1) && 
    // Height.SAHeight = Height.SafeArea && Height.SafeArea.top ? 
    // (Height.SafeArea.top > 30) ? (Height.SafeArea.top - 30) : 0 : 0;
    
    return Height
  },
  // // 获取屏幕高度 有条件
  // GetScreenHeight(params: any = {}) {
  //   let defaultHeight = uni.getStorageSync('SYSTEM_SCREEN_HEIGHT');
  //   let height = defaultHeight.ScreenHeight;
  //   let safeArea = defaultHeight.SafeArea;
  //   if(params.noSafeArea) { height -= safeArea.bottom; }
  //   if(params.noCustomBar) { height -= defaultHeight.CustomBar; }
  //   if(params.haveTabbar) { height -= uni.upx2px(100); }
    
  //   return height
  // }
  // 上传图片
  GetUploadToken() {
    return new Promise((resolve) => {
      this.Post('GetUploadToken').then((token: any) => {
        resolve(token.data || {})
      })
    })
  },
  //七牛云上传
  async UploadToQiniu(tokenData: any, file: any = []) {
    return new Promise((resolve) => {
      let fileName = 'huawx/' + Date.now() + '_' + Func.RandomString();
      uni.uploadFile({
        url: tokenData.uploadArea,
        filePath: file,
        name: 'file',
        formData: {
          'key': fileName,
          'token': tokenData.uploadToken,
        },
        // 存成功后的回调
        success: async (uploadFileRes) => {
          let key = JSON.parse(uploadFileRes.data).key;
          resolve(tokenData.uploadAddress + key);
        },
        fail: (err) => {
          // uni.showModal({
          //   title: '提示',
          //   content: '上传失败了 ' + JSON.stringify(err),
          // })
        }
      });
    })
  }
}