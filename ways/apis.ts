import Env from '../env.js';

let MainApi = Env.api;
let MainAddon = Env.addon.key;
let Addonid = Env.addon.id;
let AddonApi = MainApi + '' + MainAddon + '/';
let BaseApi = MainApi + 'common/';

export default {
	Addonid, MainAddon,
  GetUploadToken: BaseApi + 'upload/GetToken', // 获取上传的token
  BASE_AuthByOpenID: BaseApi + 'wxapp/auth/LoginByOpenID', // 使用openid登陆
  BASE_GetMockText: BaseApi + 'common/GetMockText', // 

  HUAUM_IndexGetAddonInfo: AddonApi + 'index/GetAddonInfo', // 获取应用信息
  HUAUM_SystemAboutGetInfo: AddonApi + 'system/about/GetInfo', // 获取关于信息
  HUAUM_SystemAuthAccountUpdateInfo: AddonApi + 'system/auth/AccountUpdateInfo', // 保存个人信息
  HUAUM_AdminModuleGetList: AddonApi + 'admin/module/GetList', // 获取模块列表
  HUAUM_AdminModuleGetInfo: AddonApi + 'admin/module/GetInfo', // 获取模块信息
  HUAUM_AdminModuleUpdateInfo: AddonApi + 'admin/module/UpdateInfo', // 更新模块信息
  HUAUM_AdminModuleDeleteInfo: AddonApi + 'admin/module/DeleteInfo', // 删除模块
  HUAUM_AdminModuleGetAdvanceInfo: AddonApi + 'admin/module/GetAdvanceInfo', // 获取模块高级信息
  HUAUM_AdminModuleUpdateAdvanceInfo: AddonApi + 'admin/module/UpdateAdvanceInfo', // 保存模块高级信息
  HUAUM_AdminThemeGetList: AddonApi + 'admin/theme/GetList', // 获取所有主题
  HUAUM_AdminThemeGetInfo: AddonApi + 'admin/theme/GetInfo', // 获取主题信息
  HUAUM_AdminThemeUpdateInfo: AddonApi + 'admin/theme/UpdateInfo', // 更新主题信息
  
  HUAUM_SystemTabbarGetList: AddonApi + 'system/tabbar/GetList', // 获取系统模块列表
  HUAUM_SystemTabbarFirstImport: AddonApi + 'system/tabbar/FirstImport', // 第一次将模块导入自定义导航栏
  HUAUM_SystemTabbarRemoveItem: AddonApi + 'system/tabbar/RemoveItem', // 移除自定义的导航栏
  HUAUM_SystemTabbarAddItem: AddonApi + 'system/tabbar/AddItem', // 将系统模块加入自定义导航栏
  HUAUM_SystemTabbarUpdateSort: AddonApi + 'system/tabbar/UpdateSort', // 修改自定义导航栏的排序
  HUAUM_SystemTabbarRemoveAll: AddonApi + 'system/tabbar/RemoveAll', // 移除所有自定义导航栏
  HUAUM_SystemThemeGetList: AddonApi + 'system/theme/GetList', // 获取所有主题
  HUAUM_SystemThemeSetItem: AddonApi + 'system/theme/SetItem', // 设置已选择主题
  HUAUM_AdminAboutGetInfo: AddonApi + 'admin/about/GetInfo', // 获取关于信息
  HUAUM_AdminAboutUpdateInfo: AddonApi + 'admin/about/UpdateInfo', // 更新关于信息
  
  Signup_ContentGetList: AddonApi + 'modules/signup/content/GetList', // 报名内容 获取列表
  Signup_ContentGetInfo: AddonApi + 'modules/signup/content/GetInfo', // 报名内容 获取内容
  Signup_ContentJoinItem: AddonApi + 'modules/signup/content/JoinItem', // 报名内容 加入
  Signup_ContentUnJoinItem: AddonApi + 'modules/signup/content/UnJoinItem', // 报名内容 取消加入
  Signup_ContentJoinedItem: AddonApi + 'modules/signup/content/JoinedItem', // 报名内容 我加入的
  
  Signup_AdminContentGetList: AddonApi + 'modules/signup/admin/content/GetList', // 报名内容 管理员 获取列表
  Signup_AdminContentGetInfo: AddonApi + 'modules/signup/admin/content/GetInfo', // 报名内容 管理员 获取信息
  Signup_AdminContentUpdateInfo: AddonApi + 'modules/signup/admin/content/UpdateInfo', // 报名内容 管理员 保存信息
  Signup_AdminContentDeleteInfo: AddonApi + 'modules/signup/admin/content/DeleteInfo', // 报名内容 管理员 删除信息
  Signup_AdminContentGetJoinedList: AddonApi + 'modules/signup/admin/content/GetJoinedList', // 报名内容 管理员 获取已加入的人
  
  Tangdou_TimeLineGetList: AddonApi + 'modules/tangdou/timeline/GetList', // 糖豆获取 时间轴 列表
  Tangdou_AdminTimelineGetList: AddonApi + 'modules/tangdou/admin/timeline/GetList', // 糖豆 管理员 获取 时间轴 列表
  Tangdou_AdminTimelineGetInfo: AddonApi + 'modules/tangdou/admin/timeline/GetInfo', // 糖豆 管理员 获取 时间轴 内容
  Tangdou_AdminTimelineUpdateInfo: AddonApi + 'modules/tangdou/admin/timeline/UpdateInfo', // 糖豆 管理员 更新 时间轴 内容
  Tangdou_AdminTimelineDeleteInfo: AddonApi + 'modules/tangdou/admin/timeline/DeleteInfo', // 糖豆 管理员 删除 时间轴 内容
  Tangdou_AdminSettingBasicGetInfo: AddonApi + 'modules/tangdou/admin/setting/GetInfo', // 糖豆 获取基础设置
  Tangdou_AdminSettingBasicUpdateInfo: AddonApi + 'modules/tangdou/admin/setting/UpdateInfo', // 糖豆 保存基础设置
  
  Tangdou_BigeventGetList: AddonApi + 'modules/tangdou/bigevent/GetList', // 糖豆 获取大事件
  Tangdou_AdminBigeventGetList: AddonApi + 'modules/tangdou/admin/bigevent/GetList', // 糖豆 管理员 获取大事件
  Tangdou_AdminBigeventGetInfo: AddonApi + 'modules/tangdou/admin/bigevent/GetInfo', // 糖豆 管理员 获取 大事件 内容
  Tangdou_AdminBigeventUpdateInfo: AddonApi + 'modules/tangdou/admin/bigevent/UpdateInfo', // 糖豆 管理员 更新 大事件 内容
  Tangdou_AdminBigeventDeleteInfo: AddonApi + 'modules/tangdou/admin/bigevent/DeleteInfo', // 糖豆 管理员 删除 大事件 内容
  
  Timeline_TimelineGetList: AddonApi + 'modules/timeline/timeline/GetList', // 时间轴 获取时间轴 列表
  Timeline_AdminTimelineGetList: AddonApi + 'modules/timeline/admin/timeline/GetList', // 时间轴 管理员 获取时间轴 列表
  Timeline_AdminTimelineGetInfo: AddonApi + 'modules/timeline/admin/timeline/GetInfo', // 时间轴 管理员 获取时间轴 信息
  Timeline_AdminTimelineUpdateInfo: AddonApi + 'modules/timeline/admin/timeline/UpdateInfo', // 时间轴 管理员 更新时间轴 信息
  Timeline_AdminTimelineDeleteInfo: AddonApi + 'modules/timeline/admin/timeline/DeleteInfo', // 时间轴 管理员 删除时间轴 信息
  
  Timeline_AdminTagGetList: AddonApi + 'modules/timeline/admin/tag/GetList', // 时间轴 管理员 获取时间轴 列表
  Timeline_AdminTagGetInfo: AddonApi + 'modules/timeline/admin/tag/GetInfo', // 时间轴 管理员 获取时间轴 信息
  Timeline_AdminTagUpdateInfo: AddonApi + 'modules/timeline/admin/tag/UpdateInfo', // 时间轴 管理员 更新时间轴 信息
  Timeline_AdminTagDeleteInfo: AddonApi + 'modules/timeline/admin/tag/DeleteInfo', // 时间轴 管理员 删除时间轴 信息
  // TODO: {
  //   // GetAddonInfo: AddonApi + 'index/GetAddonInfo',
  //   // GetTabbarInfo: AddonApi + 'index/GetTabbarInfo',
  //   // GetTabbarMineInfo: AddonApi + 'index/GetTabbarMineInfo', // 获取我的页面信息
  // }
  
  Payment_CommonGetInfo: AddonApi + 'modules/payment/common/GetInfo', // 支付模块获取支付信息
  Payment_CommonGenerateOrder: AddonApi + 'modules/payment/common/GenerateOrder', // 支付模块 生成订单
  Payment_CommonGetOrderInfoBeforePay: AddonApi + 'modules/payment/common/GetOrderInfoBeforePay', // 支付模块 支付前获取订单信息
  Payment_CommonOrderStartPay: AddonApi + 'modules/payment/common/OrderStartPay', // 支付模块 开始支付
  Payment_CommonOrderAfterPay: AddonApi + 'modules/payment/common/OrderAfterPay', // 支付模块 支付后获取订单信息
  Payment_CommonOrderGetList: AddonApi + 'modules/payment/common/OrderGetList', // 支付模块 获取订单列表
}